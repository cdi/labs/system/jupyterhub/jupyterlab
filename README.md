# JupyterLab

Fork of https://github.com/jupyterhub/jupyterhub-deploy-docker.
- Official JupyterHub image running on a single machine. 
- Dockerspawner for notebooks. 
- Auth is https://gitlab.rrze.fau.de/.

## HowTo: Execution:

- remember to configure the http+https proxy, if your vm is internal:
  - for a single session:
```
export HTTP_PROXY=http://proxy.rrze.uni-erlangen.de:80
export HTTPS_PROXY=http://proxy.rrze.uni-erlangen.de:80
```

- build image and run:
```
docker-compose build
docker-compose up -d
```

- additional configuration:
  - URL in traefik for each jupyterhub
  - you need to create a GitLab application and allow openid, profile and access scopes

- kill the jupyterhub
```
docker-compose down
```

