# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# Configuration file for JupyterHub
import os
import json
from oauthenticator.gitlab import LocalGitLabOAuthenticator
from oauthenticator.generic import GenericOAuthenticator

c = get_config()

# We rely on environment variables to configure JupyterHub so that we
# avoid having to rebuild the JupyterHub container every time we change a
# configuration parameter.

# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = "dockerspawner.DockerSpawner"

# Spawn containers from this image
c.DockerSpawner.image = os.environ["DOCKER_NOTEBOOK_IMAGE"]

# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.  Optionally, you can override the Docker run command
# using the DOCKER_SPAWN_CMD environment variable.
spawn_cmd = os.environ.get("DOCKER_SPAWN_CMD", "start-singleuser.sh")
c.DockerSpawner.cmd = spawn_cmd

# Connect containers to this Docker network
network_name = os.environ["DOCKER_NETWORK_NAME"]
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name

# Explicitly set notebook directory because we'll be mounting a volume to it.
# Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = os.environ.get("DOCKER_NOTEBOOK_DIR") or "/home/jovyan/work"
c.DockerSpawner.notebook_dir = notebook_dir

# Mount the real user's Docker volume on the host to the notebook user's
# notebook directory in the container
c.DockerSpawner.volumes = {"jupyterhub-user-{username}": notebook_dir}

# Remove containers once they are stopped
c.DockerSpawner.remove = True

# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True

# Limit max memory
c.DockerSpawner.mem_limit = '1G'

# Limit max cpu: cpu_quota = cpu_limit * cpu_period (100ms) per default
c.DockerSpawner.cpu_limit = 0.2

# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = "jupyterhub"
c.JupyterHub.hub_port = 8080

# Persist hub data on volume mounted inside container
c.JupyterHub.cookie_secret_file = "/data/jupyterhub_cookie_secret"
c.JupyterHub.db_url = "sqlite:////data/jupyterhub.sqlite"


# from here: https://z2jh.jupyter.org/en/stable/administrator/authentication.html#keycloak
c.JupyterHub.authenticator_class = GenericOAuthenticator

c.GenericOAuthenticator.client_id = 'jupyterhub-cdi-client'
# https://keycloak-20-0-3.eln.data.fau.de:
# c.GenericOAuthenticator.client_secret = 'fRdsslALhYTJkynknOYHm4aGhHmRXQXQ'
c.GenericOAuthenticator.client_secret = 'mnBdmT31PAp41csmXq5m4tBOWOsXsNMf'
host = 'keycloak-cdi.cloud.data.fau.de'
realm = 'jupyterhub-cdi'
c.GenericOAuthenticator.oauth_callback_url = 'https://jupyterhub-cdi.jupyter.data.fau.de/hub/oauth_callback'
c.GenericOAuthenticator.authorize_url = f'https://{host}/realms/{realm}/protocol/openid-connect/auth'
c.GenericOAuthenticator.token_url = f'https://{host}/realms/{realm}/protocol/openid-connect/token'
c.GenericOAuthenticator.userdata_url = f'https://{host}/realms/{realm}/protocol/openid-connect/userinfo'
c.GenericOAuthenticator.login_service = 'keycloak'
c.GenericOAuthenticator.username_key = 'preferred_username'
c.GenericOAuthenticator.userdata_params = {'state': 'state'}


"""
# Authenticate users with Native Authenticator
# c.JupyterHub.authenticator_class = "nativeauthenticator.NativeAuthenticator"
c.JupyterHub.authenticator_class = LocalGitLabOAuthenticator

with open('/etc/jupyterhub/gitlab_oauth_credentials.json') as f:
    gitlab_oauth = json.load(f)

c.LocalGitLabOAuthenticator.client_id = gitlab_oauth['web']['client_id']
c.LocalGitLabOAuthenticator.client_secret = gitlab_oauth['web']['client_secret']

c.LocalGitLabOAuthenticator.oauth_callback_url =  gitlab_oauth['web']['oauth_callback_url'] #'https://jupyterhub-cdi.eln.data.fau.de/hub/oauth_callback'
c.LocalGitLabOAuthenticator.create_system_users = True
c.Authenticator.add_user_cmd = ['adduser', '-q', '--gecos', '""', '--disabled-password', '--force-badname']
#c.LocalGitLabOAuthenticator.hosted_domain = 'eln.data.fau.de'   # replace with your domain
#c.LocalGitLabOAuthenticator.login_service = 'FAU'  # replace with your 'College Name'

c.LocalGitLabOAuthenticator.gitlab_url = 'https://gitlab.rrze.fau.de'
c.LocalGitLabOAuthenticator.authorize_url = 'https://gitlab.rrze.fau.de/oauth/authorize'


c.LocalGitLabOAuthenticator.scope = ['openid', 'profile', 'email', 'read_user'] # , 'api' , 'read_api'

# Allow anyone to sign-up without approval
#c.NativeAuthenticator.open_signup = True
"""
# Allowed admins
# admin = os.environ.get("JUPYTERHUB_ADMIN")
# if admin:
#    c.Authenticator.admin_users = [admin]
